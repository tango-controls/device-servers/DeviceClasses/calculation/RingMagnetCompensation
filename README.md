# RingMagnetCompensation
This is a device server that provides a calculation to obtain the current to 
compensate the SR clockwise and anti-clockwise currents. 
Pending to provide more info.


![license GPLv3+](https://img.shields.io/badge/license-GPLv3+-green.svg)

release 1.6.3

authors: Sergi Blanch-Torné, Emilio Morales

![](https://img.shields.io/badge/python-2.7-green.svg)
![](https://img.shields.io/badge/python-3.10-orange.svg)


## Release Notes

* 1.6.2: First debian release. Reformat project, add setup file.
* 1.6.1: Latest release made with Blissbuilder at ALBA Synchrotron

## Old versions
* Code migrated from [Source Forge](https://sourceforge.net/p/tango-ds/code/HEAD/tree/DeviceClasses/Calculation/RingMagnetCompensation/)

