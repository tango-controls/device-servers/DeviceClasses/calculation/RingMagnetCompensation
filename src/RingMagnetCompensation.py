#!/usr/bin/env python
# -*- coding: utf-8 -*-

# RingMagnetCompensation.py
# This file is part of tango-ds (http://sourceforge.net/projects/tango-ds/)
#
# tango-ds is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tango-ds is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with tango-ds.  If not, see <http://www.gnu.org/licenses/>.

import sys
import threading
import time
import traceback

try:
    import tango
except Exception as e:
    print("Importing PyTango as tango: {}".format(e))
    import PyTango as tango

__license__ = "GPL3+"
__author__ = "Sergi Blanch <sblanch@cells.es>"
__maintainer__ = "Emilio Morales <emorales@cells.es>"

# TODO: get traces of the current compensation changes

CLOCKWISE = "Clockwise"
COUNTERCLOCKWISE = "Counterclockwise"
DIFF = "Difference"
MAGNIFICATION = "Magnification"
OFFSET = "Offset"
COMPENSATION = "Compensation"

COMPENSATIONTRACE = "CompensationTrace"
COMPENSATIONTRACE_MAXLEN = 1000


class Attribute(object):
    def __init__(self, parent):
        self._parent = parent

    def info(self, msg):
        try:
            self._parent._group.info(msg)
        except Exception as e:
            print(("cannot print in info stream ({})".format(msg)))
            print("Exception: {}".format(e))
            print(traceback.traceback_exc())

    def debug(self, msg):
        try:
            self._parent._group.debug(msg)
        except Exception as e:
            print("cannot print in info stream ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())

    def warn(self, msg):
        try:
            self._parent._group.warn(msg)
        except Exception as e:
            print("cannot print in info stream ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())

    def error(self, msg):
        try:
            self._parent._group.error(msg)
        except Exception as e:
            print("cannot print in info stream ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())


class AttrEvent(Attribute):
    def __init__(self, parent):
        # super(AttrEvent,self).__init__(parent)
        Attribute.__init__(self, parent)  # self._parent = parent

    def push_event(self, event):
        try:
            if event is None:
                return
            self.debug(
                "PushEvent() {} = {:6.3f}".format(
                    event.attr_name, event.attr_value.value
                )
            )
            self._parent._attrValue = event.attr_value.value
            self._parent.pushUpdate()
        except Exception as e:
            self.debug(
                "{}.PushEvent() Exception: {}".format(self._parent._devName, e)
            )
            try:  # in case of event problem, read it "manually"
                value = self._parent._devProxy.read_attribute(
                    self._parent._attrName
                ).value
                if value != self._parent._attrValue:
                    self._parent._attrValue = value
                    self._parent.pushUpdate()
            except Exception as e:
                self.error(
                    "{}.PushEvent() secondary Exception on "
                    "manual retry: {}".format(self._parent._devName, e)
                )


class AttrThread(Attribute):
    def __init__(self, parent):
        Attribute.__init__(self, parent)  # self._parent = parent
        # create a thread to periodically read the attribute
        self._t = 1
        self._joinerEvent = threading.Event()  # to communicate between threads
        self._joinerEvent.clear()
        self._thread = threading.Thread(target=self.read_attr)
        self._thread.setDaemon(True)
        self._thread.start()

    def read_attr(self):
        while not self._joinerEvent.isSet():
            #            print("%s/%s = %6.3f"%(self._parent._devName,
            #                                   self._parent._attrName,
            #                                   self._parent._attrValue))
            try:
                value = self._parent._devProxy.read_attribute(
                    self._parent._attrName
                ).value
                if not self._parent._attrValue == value:
                    self.debug(
                        "read_attr: {}/{} = {:6.3f}".format(
                            self._parent._devName, self._parent._attrName,
                            value
                        )
                    )
                    self._parent._attrValue = value
                    self._parent.pushUpdate()
                time.sleep(self._t)
            except Exception as e:
                self.error(
                    "Cannot read {}/{}, due to {}".format(
                        self._parent._devName, self._parent._attrName, e
                    )
                )
                time.sleep(1)  # FIXME: this time would be variable
        self.debug(
            "JoinerEvent is set finishing thread for {}/{}".format(
                self._parent._devName, self._parent._attrName
            )
        )

    def setSleepTime(self, t):
        if t != self._t:
            self.debug(
                "Set the sleep time to read attr to {:6.3f} seconds for {} "
                "(was {:6.3f})".format(tm, self._parent._devName, self._t)
            )
            self._t = t


class Magnet:
    def __init__(self, idx, devName, devProxy, attrName, group):
        self._idx = idx
        self._devName = devName
        self._devProxy = devProxy
        self._attrName = attrName
        self._group = group
        self._attrValue = 0
        self._group.append(self)
        try:
            self._attrManager = AttrEvent(self)
            self._eventID = self._devProxy.subscribe_event(
                self._attrName, tango.EventType.CHANGE_EVENT, self._attrManager
            )
            self.info("{} running with event monitor".foramt(self._devName))
        except Exception as e:
            self._attrManager = AttrThread(self)
            self._eventID = None
            self.info("{} running with thread monitor".format(self._devName))
            print(
                "Exception at __init__: {}\n{}".format(
                    e, traceback.format_exc()
                )
            )

    def __del__(self):
        self.debug("Delete Magnet Manager for {}".format(self._devName))
        if self._eventID is None:
            self._attrManager._joinerEvent.set()
        else:
            self._devProxy.unsubscribe_event(self._eventID)

    def pushUpdate(self):
        self.info(
            "{} pushUpdate() new value {:6.3f} (was {:6.3f})".format(
                self._devName, self._attrValue,
                self._group._currents[self._idx]
            )
        )
        try:
            # First level of filtering:
            if self._devProxy["isCycling"].value:
                self.warn(
                    "{} pushUpdate() ignoring new value because "
                    "it's cycling.".format(self._devName)
                )
                return
            if self._devProxy["state"].value not in [
                tango.DevState.ON,
                tango.DevState.MOVING,
            ]:
                self.warn(
                    "{} pushUpdate() ignoring new value because "
                    "it's not powered on ({}).".format(
                        self._devName, self._devProxy['state'].value
                    )
                )
                if self._group._currents[self._idx] != 0:
                    self._group.pushUpdate(self._idx, 0.0)
                return
            # Second level of filtering
            if (
                abs(self._attrValue - self._group._currents[self._idx]) < 0.1
            ):  # FIXME: this is a constant that must be described on top or
                # convert to a modifiable vble.
                self.warn(
                    "{} pushUpdate() ignoring new value. "
                    "Change is too small.".format(self._devName)
                )
                return
            # Time to apply if need be.
            self._group.pushUpdate(self._idx, self._attrValue)
        except Exception as e:
            self.error(
                "Exception pushing {} current value".format(self._devName)
            )
            self.error("Exception: {}".format(e))
            self.error(traceback.format_exc())

    def getName(self):
        return self._devName

    def getValue(self):
        return self._attrValue

    def info(self, msg):
        self._group.info(msg)

    def debug(self, msg):
        self._group.debug(msg)

    def warn(self, msg):
        self._group.warn(msg)

    def error(self, msg):
        self._group.error(msg)

    def setSleepTime(self, t):
        if self._eventID is None:
            self._attrManager.setSleepTime(t)


class MagnetGroup:
    def __init__(self, parent, roleName):
        self._parent = parent
        self._role = roleName
        self._magnets = []
        self._names = []
        self._currents = []
        self._summatory = 0

    def append(self, magnet):
        self._magnets.append(magnet)
        self._names.append(magnet.getName())
        self._currents.append(magnet.getValue())

    def __len__(self):
        return len(self._magnets)

    def pushUpdate(self, idx, value):
        self.debug(
            "%s pushUpdate(id=%d) have an update: %s from %6.3f to %6.3f"
            % (self._role, idx, self._names[idx], self._currents[idx], value)
        )
        self._summatory -= self._currents[idx]  # minus the old value
        self._currents[idx] = value
        self._summatory += self._currents[idx]  # plus the new value
        self._parent.generateEvents(self._role)

    def getNames(self):
        return self._names

    def getCurrents(self):
        return self._currents

    def getSummatory(self):
        return self._summatory

    def info(self, msg):
        try:
            self._parent._setCompensationTrace("(info)" + msg)
        except Exception as e:
            print("cannot print in compensationTrace ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())
        try:
            self._parent.info_stream(msg)
        except Exception as e:
            print("cannot print in compensationTrace ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())

    def debug(self, msg):
        try:
            self._parent._setCompensationTrace("(debug)" + msg)
        except Exception as e:
            print("cannot print in compensationTrace ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())
        try:
            self._parent.debug_stream(msg)
        except Exception as e:
            print("cannot print in compensationTrace ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())

    def warn(self, msg):
        try:
            self._parent._setCompensationTrace("(warn)" + msg)
        except Exception as e:
            print("cannot print in compensationTrace ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())
        try:
            self._parent.warn_stream(msg)
        except Exception as e:
            print("cannot print in compensationTrace ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())

    def error(self, msg):
        try:
            self._parent._setCompensationTrace("(error)" + msg)
        except Exception as e:
            print("cannot print in compensationTrace ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())
        try:
            self._parent.error_stream(msg)
        except Exception as e:
            print("cannot print in compensationTrace ({})".format(msg))
            print("Exception: {}".format(e))
            print(traceback.format_exc())


class RingMagnetCompensation(tango.LatestDeviceImpl):
    def __init__(self, cl, name):
        tango.LatestDeviceImpl.__init__(self, cl, name)
        self._compensation_trace = []
        RingMagnetCompensation.init_device(self)

    def init_device(self):
        self.debug_stream("In {} ::init_device()".format(self.get_name()))
        self.change_state(tango.DevState.INIT)
        self.get_device_properties(self.get_device_class())

        self._important_logs = []

        self.set_change_event("State", True, False)
        self.set_change_event("Status", True, False)

        self._data = {"allMagnets": []}

        self._data[CLOCKWISE] = MagnetGroup(self, CLOCKWISE)
        self._data[COUNTERCLOCKWISE] = MagnetGroup(self, COUNTERCLOCKWISE)
        self.set_change_event(CLOCKWISE, True, False)
        self.set_change_event("{}DoubleList".format(CLOCKWISE), True, False)
        self.set_change_event("{}StringList".format(CLOCKWISE), True, False)
        self.set_change_event(COUNTERCLOCKWISE, True, False)
        self.set_change_event("{}DoubleList".format(COUNTERCLOCKWISE), True,
                              False)
        self.set_change_event("{}StringList".format(COUNTERCLOCKWISE), True,
                              False)

        self._data[MAGNIFICATION] = 1
        self.set_change_event(MAGNIFICATION, True, False)
        self._data[OFFSET] = 0
        self.set_change_event(OFFSET, True, False)

        self._data[DIFF] = 0
        self.set_change_event(DIFF, True, False)
        self._data[COMPENSATION] = 0
        self.set_change_event(COMPENSATION, True, False)
        self._compensationProxy = None

        self.change_state(tango.DevState.OFF)
        if self.StartUpState.lower() in ["standby", "on"]:
            self.monitorMagnets()
            self.change_state(tango.DevState.STANDBY)
            self.addStatusMsg(
                "The calculated current is not being applied", True
            )
        if self.StartUpState.lower() in ["on"]:
            self.connect2Compensator()

    def connect2Compensator(self):
        try:
            self._compensationProxy = tango.DeviceProxy(
                self.CompensationMagnet
            )
            # TODO: dynattr of the compensation magnet important attributes
            self.CalculateCompensation()
        except Exception as e:
            self._compensationProxy = None
            msg = "No property found to write the compensation current"
            self.info_stream(msg)
            self.addStatusMsg(msg, True)
            self.error_stream(
                "Exception at connect2Compensator method: {}".format(e))
            self.error_stream(traceback.format_exc())

    def disconnect2Compensator(self):
        self._compensationProxy = None

    def monitorMagnets(self):
        try:
            self.unmonitorMagnets()  # FIXME: do it only if need
            for property, role in [
                (self.Clockwise, CLOCKWISE),
                (self.Counterclockwise, COUNTERCLOCKWISE),
            ]:
                if len(property) == 0:
                    raise Exception(
                        "Bad configuration in the %s property!" % role
                    )
                if len(property) == 1 and len(str(property).split(" ")) > 1:
                    property = str(property).split(" ")
                for i, devName in enumerate(property):
                    try:
                        # check the existence of the magnet device
                        try:
                            devProxy = tango.DeviceProxy(devName)
                        except Exception as e:
                            self.addStatusMsg(
                                "Cannot bind proxy %s in the %s property"
                                % (devName, role),
                                True,
                            )
                            self.error_stream("Exception: {}".format(e))
                            self.error_stream(traceback.format_exc())
                        # subscribe to current attribute
                        else:
                            # create a magnet encapsulated object
                            magnet = Magnet(
                                len(self._data[role]),
                                # the sequence id in the list
                                devName,
                                devProxy,
                                self.MagnetsAttr,
                                self._data[role],
                            )
                            if magnet._eventID is None:
                                self.info_stream(
                                    "Magnet {} in threading mode".format(
                                        devName
                                    )
                                )
                                magnet.setSleepTime(self.AttrThreadSleep)
                            else:
                                self.info_stream(
                                    "Magnet {} in event mode".format(devName)
                                )
                            # put vbles on the places
                            self._data["allMagnets"].append(magnet)
                    except Exception as e:
                        msg = "Cannot include {} {}".format(role, devName)
                        self.warn_stream("{}, due to {}".format(msg, e))
                        self.addStatusMsg(msg, True)
        except Exception as e:
            self.error_stream(
                "In {}::init_device(): Exception: {}.".format(
                    self.get_name(), e
                )
            )
            self.change_state(tango.DevState.FAULT)
            self.addStatusMsg("Init Exception: {}".format(e), True)
        else:
            if self._compensationProxy is None:
                self.change_state(tango.DevState.STANDBY)
            else:
                self.change_state(tango.DevState.ON)
            self.addStatusMsg(" ")

    def unmonitorMagnets(self):
        self.disconnect2Compensator()
        for magnet in self._data["allMagnets"]:
            del magnet
        self._data[CLOCKWISE] = MagnetGroup(self, CLOCKWISE)
        self._data[COUNTERCLOCKWISE] = MagnetGroup(self, COUNTERCLOCKWISE)

    def generateEvents(self, role):
        self.CalculateCompensation()
        self.fireEventsList(
            [
                [role + "Doublelist", self._data[role].getCurrents()],
                [role, self._data[role].getSummatory()],
                [DIFF, self._data[DIFF]],
                [COMPENSATION, self._data[COMPENSATION]],
            ]
        )

    def read_ClockwiseDoubleList(self, attr):
        self.info_stream(
            "In {}::read_ClockwiseDoubleList()".format(self.get_name())
        )
        attr_Clockwiselist = self._data[CLOCKWISE].getCurrents()
        attr.set_value(attr_Clockwiselist, len(attr_Clockwiselist))

    def read_ClockwiseStringList(self, attr):
        self.info_stream(
            "In {}::read_ClockwiseStringList()".format(self.get_name())
        )
        attr_Clockwiselist = self._data[CLOCKWISE].getNames()
        attr.set_value(attr_Clockwiselist, len(attr_Clockwiselist))

    def read_CounterclockwiseDoubleList(self, attr):
        self.info_stream(
            "In {}::read_CounterclockwiseDoubleList()".format(self.get_name())
        )
        attr_Counterclockwiselist = self._data[COUNTERCLOCKWISE].getCurrents()
        attr.set_value(
            attr_Counterclockwiselist, len(attr_Counterclockwiselist)
        )

    def read_CounterclockwiseStringList(self, attr):
        self.info_stream(
            "In {}::read_CounterclockwiseStringList()".format(self.get_name())
        )
        attr_Counterclockwiselist = self._data[COUNTERCLOCKWISE].getNames()
        attr.set_value(
            attr_Counterclockwiselist, len(attr_Counterclockwiselist)
        )

    def read_Clockwise(self, attr):
        self.info_stream("In {}::read_Clockwise()".format(self.get_name()))
        attr.set_value(self._data[CLOCKWISE].getSummatory())

    def read_Counterclockwise(self, attr):
        self.info_stream(
            "In {}::read_Counterclockwise()".format(self.get_name())
        )
        attr.set_value(self._data[COUNTERCLOCKWISE].getSummatory())

    def read_Magnification(self, attr):
        self.info_stream("In {}::read_Magnification()".format(self.get_name()))
        attr.set_value(self._data[MAGNIFICATION])

    def write_Magnification(self, attr):
        data = []
        attr.get_write_value(data)
        self.info_stream("Attribute value = {}".format(data))
        #    Add your own code here
        self._setCompensationTrace(
            "Magnification: was {:6.3f}, new value {:6.3f}".format(
                self._data[MAGNIFICATION], data[0]
            )
        )
        self._data[MAGNIFICATION] = data[0]

        self.CalculateCompensation()
        self.fireEventsList(
            [
                [MAGNIFICATION, self._data[MAGNIFICATION]],
                [COMPENSATION, self._data[COMPENSATION]],
            ]
        )

    def read_Offset(self, attr):
        self.info_stream("In {}::read_Offset()".format(self.get_name()))
        attr.set_value(self._data[OFFSET])

    def write_Offset(self, attr):
        data = []
        attr.get_write_value(data)
        self.debug_stream("Attribute value = {}".format(data))
        #    Add your own code here
        self._setCompensationTrace(
            "Offset: was {:6.3f}, new value {:6.3f}".format(
                self._data[OFFSET], data[0]
            )
        )
        self._data[OFFSET] = data[0]
        self.CalculateCompensation()
        self.fireEventsList(
            [
                [OFFSET, self._data[OFFSET]],
                [COMPENSATION, self._data[COMPENSATION]],
            ]
        )

    def read_Difference(self, attr):
        self.info_stream(
            "In {}::read_Difference()".format(self.get_name())
        )

        #    Add your own code here
        # self.calculateCompensation()
        attr.set_value(self._data[DIFF])

    def read_Compensation(self, attr):
        self.info_stream(
            "In {}::read_Compensation()".format(self.get_name())
        )

        #    Add your own code here
        # self.calculateCompensation()
        attr.set_value(self._data[COMPENSATION])

    def read_CompensationTrace(self, attr):
        if len(self._compensation_trace) > COMPENSATIONTRACE_MAXLEN:
            idx = len(self._compensation_trace) - COMPENSATIONTRACE_MAXLEN
            self._compensation_trace = self._compensation_trace[idx:]
        attr.set_value(self._compensation_trace)

    def _setCompensationTrace(self, new_trace):
        msg = "{} {}".format(time.strftime('%Y%m%d_%H%M%S:'), new_trace)
        self._compensation_trace.append(msg)
        self.debug_stream(msg)
        if len(self._compensation_trace) > COMPENSATIONTRACE_MAXLEN:
            idx = len(self._compensation_trace) - COMPENSATIONTRACE_MAXLEN
            self._compensation_trace = self._compensation_trace[idx:]

    #        while len(self._compensation_trace) > COMPENSATIONTRACE_MAXLEN:
    #            self._compensation_trace.pop(0)
    def _cleanCompensationTrace(self):
        self._compensation_trace = []

    # ==================================================================
    #
    #   RingMagnetCompensation.py command methods
    #
    # ==================================================================

    # ------------------------------------------------------------------
    #   Stop command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def Stop(self):
        self.info_stream("In {}::Stop()".format(self.get_name()))
        #   Add your own code here
        self.cleanAllImportantLogs()
        self.unmonitorMagnets()
        self.change_state(tango.DevState.OFF)
        self.addStatusMsg(" ")
        self.fireEventsList([[DIFF, 0.0], [COMPENSATION, 0.0]])

    # ------------------------------------------------------------------
    #   Standby command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def Standby(self):
        self.info_stream("In {}::Standby()".format(self.get_name()))
        #   Add your own code here
        self.cleanAllImportantLogs()
        if self.get_state() in [tango.DevState.OFF]:
            self.monitorMagnets()
        elif self.get_state() in [tango.DevState.ON]:
            self.disconnect2Compensator()
        else:
            return  # FIXME: ??
        self.CalculateCompensation()
        self.change_state(tango.DevState.STANDBY)
        self.addStatusMsg("The calculated current is not being applied", True)

    # ------------------------------------------------------------------
    #   Start command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def Start(self):
        self.info_stream("In {}::Start()".format(self.get_name()))
        #   Add your own code here
        state = self.get_state()
        if state in [tango.DevState.OFF, tango.DevState.STANDBY]:
            self.cleanAllImportantLogs()
            if state not in [tango.DevState.STANDBY]:
                self.monitorMagnets()
            self.connect2Compensator()
            self.change_state(tango.DevState.ON)
            self.CalculateCompensation()
        if self._compensationProxy is not None:
            self.change_state(self._compensationProxy.state())
            self.addStatusMsg(" ")

    # ------------------------------------------------------------------
    #   CalculateCompensation command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def CalculateCompensation(self):
        clk = self._data[CLOCKWISE].getSummatory()
        counterclk = self._data[COUNTERCLOCKWISE].getSummatory()
        self._data[DIFF] = counterclk - clk
        self._data[COMPENSATION] = (
            self._data[DIFF] * self._data[MAGNIFICATION]
        ) + self._data[OFFSET]
        if self._compensationProxy is None:
            pass  # Only connected when is applying the calculation
        else:
            try:
                self.getCompensationMagnetStatus()
                if self.get_state() in [
                    tango.DevState.ON,
                    tango.DevState.MOVING,
                    tango.DevState.ALARM,
                ]:
                    self.change_state(tango.DevState.MOVING)
                    try:
                        currentInThePS = (
                            self._compensationProxy.read_attribute(
                                "CurrentSetpoint"
                            ).value
                        )
                        if self._data[COMPENSATION] != currentInThePS:
                            msg = (
                                "Update compensation current from {:6.3f} to "
                                "{:6.3f}".format(
                                    currentInThePS, self._data[COMPENSATION]
                                )
                            )
                            self._setCompensationTrace(msg)
                            self._compensationProxy.write_attribute(
                                "CurrentSetpoint", self._data[COMPENSATION]
                            )
                    except Exception as e:
                        self._setCompensationTrace(
                            "Cannot update compensation current: %s" % e
                        )
                        self.change_state(tango.DevState.ALARM)
                        self.addStatusMsg(
                            "Cannot write the compensation current to "
                            "{}".format(self.CompensationMagnet), True,
                        )
                    else:
                        self.change_state(tango.DevState.ON)
            except Exception as e:
                self.change_state(tango.DevState.ALARM)
                self.addStatusMsg(
                    "Cannot calculate the compensation current to %s"
                    % self.CompensationMagnet
                )
                self.error_stream(
                    "Exception at calculateCompensation: {}".format(e)
                )
                self.error_stream(traceback.format_exc())
        events2fire = [
            [DIFF, self._data[DIFF]],
            [COMPENSATION, self._data[COMPENSATION]],
        ]
        self.fireEventsList(events2fire)

    # ------------------------------------------------------------------
    #   ResetInterlocks command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def ResetInterlocks(self):
        self.info_stream("In {}::ResetInterlocks()".format(self.get_name()))
        #  Add your own code here
        try:
            if self._compensationProxy is not None:
                self._compensationProxy.ResetInterlocks()
                self.disconnect2Compensator()
            self.unmonitorMagnets()
            if self.get_state() in [tango.DevState.STANDBY, tango.DevState.ON]:
                self.monitorMagnets()
                if self.get_state() in [tango.DevState.ON]:
                    self.connect2Compensator()
        except Exception as e:
            self.addStatusMsg("ResetInterlocks error", True)
            self.error_stream("Exception at ResetInterlocks: {}".format(e))
            self.error_stream(traceback.format_exc())

    # ------------------------------------------------------------------
    #   Off command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def Off(self):
        self.info_stream("In {}::Off()".format(self.get_name()))
        #  Add your own code here
        try:
            self.Stop()
        #            if not self._compensationProxy == None:
        #                self._compensationProxy.Off()
        except Exception as e:
            self.addStatusMsg("Off error", True)
            self.error_stream("Exception in Off method: {}".format(e))
            self.error_stream(traceback.format_exc())

    # ------------------------------------------------------------------
    #   On command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def On(self):
        self.info_stream("In {}::On()".format(self.get_name()))
        #  Add your own code here
        try:
            self.Start()
        #            if not self._compensationProxy == None:
        #                self._compensationProxy.On()
        except Exception as e:
            self.addStatusMsg("On error", True)
            self.error_stream("Exception in On method: {}".fromat(e))
            self.error_stream(traceback.format_exc())

    # auxiliar methods
    def change_state(self, newstate):
        self.info_stream(
            "In {}::change_state({})".format(self.get_name(), str(newstate))
        )
        if newstate != self.get_state():
            self.set_state(newstate)
            self._setCompensationTrace("ChangeState to {}".format(ewstate))
            self.push_change_event("State", newstate)

    def fireEventsList(self, eventsAttrList):
        timestamp = time.time()
        for attrEvent in eventsAttrList:
            try:
                self.info_stream(
                    "In {}::fireEventsList() attribute: {}".format(
                        self.get_name(), attrEvent[0]
                    )
                )
                if len(attrEvent) == 3:  # specifies quality
                    self.push_change_event(
                        attrEvent[0], attrEvent[1], timestamp, attrEvent[2]
                    )
                else:
                    self.push_change_event(
                        attrEvent[0],
                        attrEvent[1],
                        timestamp,
                        tango.AttrQuality.ATTR_VALID,
                    )
            except Exception as e:
                self.error_stream(
                    "Exception with attribute {}".format(attrEvent[0])
                )
                self.error_stream("Exception at fireEventsList: {}".format(e))
                self.error_stream(traceback.format_exc())

    def cleanAllImportantLogs(self):
        self.info_stream(
            "In {}::cleanAllImportantLogs()".format(self.get_name())
        )
        self._important_logs = []
        self.addStatusMsg("")

    def addStatusMsg(self, current, important=False):
        self._setCompensationTrace("append to status: '{}'".format(current))
        self.info_stream("In {}::addStatusMsg()".format(self.get_name()))
        msg = ""  # The device is in %s state.\n"%(self.get_state())
        for ilog in self._important_logs:
            msg = "{}{}\n".format(msg, ilog)
        status = "{}{}".format(msg, current)
        self.set_status(status)
        self.push_change_event("Status", status)
        if important and current not in self._important_logs:
            self._important_logs.append(current)

    def getCompensationMagnetStatus(self):
        msg = (
            "{} in on {} state".format(
                self.CompensationMagnet, self._compensationProxy.state()
            )
        )
        compensationStatus = self._compensationProxy.Status()
        for line in compensationStatus.split("\n")[1:]:
            if len(line) > 0:
                msg += "\n{}".format(line)
        self.info_stream(msg)
        self.cleanAllImportantLogs()
        self.addStatusMsg(msg, True)


#    def buildCompensationDynAttrs(self):
#        if not self._compensationProxy is None:
#            check = [
#                "Current","CurrentSetpoint","Voltage","StateCode64","RemoteMode"
#            ]
#            for attrName in check:
#                if attrName not in self._compensationProxy.get_attribute_list(): # noqa: E501
#                    msg = "Cannot create dynAttr {}, " \
#                          "is not in the subdevice"(attrName)
#                    self.info_stream(msg)
#                    self.addStatusMsg(msg)
#                else:
#                    attrProxy = tango.AttributeProxy(
#                        self.CompensationMagnet+"/"+attrName
#                    )
#                    attr = tango.Attr(attrName)
#                    #TODO...
#    def read_dynattr(self,attr):
#        pass
#    def destroyCompensationDynAttrs(self):
#        pass
# end auxiliar methods
##


class RingMagnetCompensationClass(tango.DeviceClass):
    #    Class Properties
    class_property_list = {}

    #    Device Properties
    device_property_list = {
        "Clockwise": [
            tango.DevVarStringArray,
            "List of device servers in the clockwise",
            [],
        ],
        "Counterclockwise": [
            tango.DevVarStringArray,
            "List of device servers in the counterclockwise",
            [],
        ],
        "MagnetsAttr": [
            tango.DevString,
            "Name of the attribute to subscribe to the magnets",
            [],
        ],
        "CompensationMagnet": [
            tango.DevString,
            "Magnet name where the compensation will be written",
            [],
        ],
        "AttrThreadSleep": [
            tango.DevDouble,
            "Expert property to fine adjust of the refresh on the threaded "
            "option of the magnets current refresh",
            [1],
        ],
        "StartUpState": [
            tango.DevString,
            "During the start up procedure, the state evolution can be "
            "stopped in one of the steps: off,standby,on",
            ["On"],
        ],
    }

    #    Command definitions
    cmd_list = {
        "Stop": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "Standby": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "Start": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "CalculateCompensation": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        # Compensator proxy commands mapped
        "ResetInterlocks": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "On": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "Off": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
    }

    #    Attr definitions
    attr_list = {
        # FIXME: next 2 attrs should be one DevVarDoubleStringArray
        "{}DoubleList".format(CLOCKWISE): [
            [tango.DevDouble, tango.SPECTRUM, tango.READ, 1000],
            {
                "label": "Clockwise magnet currents list",
                "unit": "A",
                "description": "list of the magnet currents in the clockwise "
                "magnets",
            },
        ],
        "{}StringList".format(CLOCKWISE): [
            [tango.DevString, tango.SPECTRUM, tango.READ, 1000],
            {
                "label": "Clockwise magnet names list",
                "unit": "A",
                "description": "list of the magnet names in the "
                "clockwise magnets",
            },
        ],
        CLOCKWISE: [
            [tango.DevDouble, tango.SCALAR, tango.READ],
            {
                "label": "Clockwise summatory",
                "unit": "A",
                "description": "clockwise current addition",
            },
        ],
        # FIXME: next 2 attrs should be one DevVarDoubleStringArray
        "{}DoubleList".format(COUNTERCLOCKWISE): [
            [tango.DevDouble, tango.SPECTRUM, tango.READ, 1000],
            {
                "label": "Counterclockwise magnet currents list",
                "unit": "A",
                "description": "list of the magnet currents in the "
                "counterclockwise magnets",
            },
        ],
        "{}StringList".format(COUNTERCLOCKWISE): [
            [tango.DevString, tango.SPECTRUM, tango.READ, 1000],
            {
                "label": "Counterclockwise magnet names list",
                "unit": "A",
                "description": "list of the magnet names in the "
                "counterclockwise magnets",
            },
        ],
        COUNTERCLOCKWISE: [
            [tango.DevDouble, tango.SCALAR, tango.READ],
            {
                "label": "Counterclockwise summatory",
                "unit": "A",
                "description": "counterclockwise current addition",
            },
        ],
        DIFF: [
            [tango.DevDouble, tango.SCALAR, tango.READ],
            {
                "label": "Difference",
                "description": "Difference between clockwise and "
                "counterclockwise magnets",
            },
        ],
        MAGNIFICATION: [
            [tango.DevDouble, tango.SCALAR, tango.READ_WRITE],
            {
                "label": "Magnification Factor",
                "description": "Magnification of the output current",
                "Memorized": "true",
            },
        ],
        OFFSET: [
            [tango.DevDouble, tango.SCALAR, tango.READ_WRITE],
            {
                "label": "Offset",
                "description": "Linear shift to the output",
                "Memorized": "true",
            },
        ],
        COMPENSATION: [
            [tango.DevDouble, tango.SCALAR, tango.READ],
            {
                "label": "Current Compensation",
                "unit": "A",
                "description": "Clockwise current to compensate ring magnets",
            },
        ],
        COMPENSATIONTRACE: [
            [
                tango.DevString,
                tango.SPECTRUM,
                tango.READ,
                COMPENSATIONTRACE_MAXLEN,
            ],
            {"description": "Collect the logs of the compensation."},
        ],
    }

    def __init__(self, name):
        tango.DeviceClass.__init__(self, name)
        self.set_type(name)
        print("In RingMagnetCompensation_Class  constructor")


def main():
    try:
        py = tango.Util(sys.argv)
        py.add_TgClass(
            RingMagnetCompensationClass,
            RingMagnetCompensation,
            "RingMagnetCompensation",
        )

        U = tango.Util.instance()
        U.server_init()
        U.server_run()

    except tango.DevFailed as e:
        print("-------> Received a DevFailed exception:{}".format(e))
    except Exception as e:
        print("-------> An unforeseen exception occurred....{}".format(e))


if __name__ == "__main__":
    main()
