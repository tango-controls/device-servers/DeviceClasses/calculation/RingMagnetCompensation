# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>
#
# ##### END GPL LICENSE BLOCK #####

from setuptools import setup, find_packages


__author__ = "Sergi Blanch-Torné"
__maintainer__ = "Emilio Morales"
__email__ = "emorales@cells.es"
__copyright__ = "Copyright 2022, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

__project__ = "tangods-ringmagnetcompensation"
__description__ = "Python module that calculate magnet current compensation"
__longDesc__ = """
 This module has been prepared to calculate the necessary
 current to be applied at Danfysik 9100 power supply to compensate current
 differences between clockwise and anti-clockwise circuits from SR power
 converters.
"""
__url__ = (
    "https://gitlab.com/tango-controls/device-servers/DeviceClasses"
    "/calculation/RingMagnetCompensation "
)

__version__ = "__version__ = __version__ = 1.6.3"""


setup(
    name=__project__,
    license=__license__,
    description=__description__,
    long_description=__longDesc__,
    version=__version__,
    author=__author__,
    author_email=__email__,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: "
        "GNU General Public License "
        "v3 or later (GPLv3+)",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Topic :: Software Development :: Embedded Systems",
        "Topic :: Software Development :: Libraries :: " "Python Modules",
    ],
    packages=find_packages(),
    url=__url__,
    entry_points={
        "console_scripts": [
            "RingMagnetCompensation=src.RingMagnetCompensation:main"
        ]
    },
)

# for the classifiers review see:
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
#
# Development Status :: 1 - Planning
# Development Status :: 2 - Pre-Alpha
# Development Status :: 3 - Alpha
# Development Status :: 4 - Beta
# Development Status :: 5 - Production/Stable
